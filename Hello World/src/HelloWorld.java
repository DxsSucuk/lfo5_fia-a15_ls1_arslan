import java.util.Scanner;

public class HelloWorld {

	public static void main(String[] args) {
		System.out.println("Namen bitte eintippen!");
		
		Scanner inputScanner = new Scanner(System.in);
		
		System.out.println("\nGuten Tag " + inputScanner.next() + "\nWillkommen in der Veranstaltung Strukturierte Programmierung.");
		
		inputScanner.close();
	}

}
