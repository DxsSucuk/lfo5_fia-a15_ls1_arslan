package de.presti.oszimt.raumschiff.entities;

import java.util.ArrayList;
import java.util.Date;

public class Pilot {

    private String name;
    private Date kontrolleSeit;
    private final ArrayList<LogbuchEintrag> logbuchEintraege = new ArrayList<>();

    public Pilot(String name, Date kontrolleSeit) {
        this.name = name;
        this.kontrolleSeit = kontrolleSeit;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getKontrolleSeit() {
        return kontrolleSeit;
    }

    public void setKontrolleSeit(Date kontrolleSeit) {
        this.kontrolleSeit = kontrolleSeit;
    }

    public ArrayList<LogbuchEintrag> getLogbuchEintraege() {
        return logbuchEintraege;
    }

    public boolean addLogbuchEintrag(LogbuchEintrag logbuchEintrag) {
        return !getLogbuchEintraege().contains(logbuchEintrag) && getLogbuchEintraege().add(logbuchEintrag);
    }

    public boolean removeLogbuchEintrag(LogbuchEintrag logbuchEintrag) {
        return getLogbuchEintraege().contains(logbuchEintrag) && getLogbuchEintraege().remove(logbuchEintrag);
    }

    @Override
    public String toString() {
        return "Pilot{" +
                "name='" + name + '\'' +
                ", kontrolleSeit=" + kontrolleSeit +
                ", logbuchEintraege=" + logbuchEintraege +
                '}';
    }
}
