package de.presti.oszimt.raumschiff.main;

import de.presti.oszimt.raumschiff.entities.Pilot;
import de.presti.oszimt.raumschiff.entities.Raumschiff;

import java.util.Date;

public class Main {

    public static void main(String[] args) {
        Raumschiff raumschiff = new Raumschiff("Cumbuster 3000", new Pilot("Jeff", new Date()),
                                                100, 500, 100,
                                                    5, 10);

        Raumschiff raumschiff2 = new Raumschiff("NuklearCummer", new Pilot("Jeff 2.0", new Date()),
                100, 500, 100,
                5, 15);

        Raumschiff raumschiff3 = new Raumschiff("Cumminommi", new Pilot("Jeff 3.0", new Date()),
                100, 500, 100,
                5, 20);

        raumschiff.schiessen(raumschiff2);

        raumschiff2.schiessenTorpedos(raumschiff, 3);

        raumschiff3.schiessenTorpedos(raumschiff2, 4);

        System.out.println("Status des Schiffes von " + raumschiff.getPilot().getName() + ", " + raumschiff.getStatus());
        System.out.println("Status des Schiffes von " + raumschiff2.getPilot().getName() + ", " + raumschiff2.getStatus());
        System.out.println("Status des Schiffes von " + raumschiff3.getPilot().getName() + ", " + raumschiff3.getStatus());

        if (raumschiff.kannGeplundertWerden()) {
            System.out.println(raumschiff.getName() + " wurde von " + raumschiff3.getName() + " geplündert!");
            raumschiff3.plundern(raumschiff);
        } else {
            System.out.println(raumschiff.getName() + " hat sich nach einem Plünder versuch gegen " + raumschiff2.getName() + " gewehrt!");
            raumschiff.schiessenTorpedos(raumschiff2, raumschiff.getTorpedos());
        }

        if (raumschiff2.kannGeplundertWerden()) {
            System.out.println(raumschiff2.getName() + " wurde von " + raumschiff3.getName() + " geplündert!");
            raumschiff3.plundern(raumschiff2);
        } else {
            System.out.println(raumschiff2.getName() + " hat sich nach einem Plünder versuch gegen " + raumschiff3.getName() + " gewehrt!");
            raumschiff2.schiessenTorpedos(raumschiff3, 15);
        }

        System.out.println("Status des Schiffes von " + raumschiff.getPilot().getName() + ", " + raumschiff.getStatus());
        System.out.println("Status des Schiffes von " + raumschiff2.getPilot().getName() + ", " + raumschiff2.getStatus());
        System.out.println("Status des Schiffes von " + raumschiff3.getPilot().getName() + ", " + raumschiff3.getStatus());
    }

}
