package de.presti.oszimt.raumschiff.entities;

public class StatusReport {

    private double energieInProzent, schildInProzent, lifeSupportInProzent;

    public StatusReport(Raumschiff raumschiff) {
        this(raumschiff.getEnergieVersorgungInProzent(), raumschiff.getSchildInProzent(), raumschiff.getLifeSupportInProzent());
    }

    public StatusReport(double energieInProzent, double schildInProzent, double lifeSupportInProzent) {
        this.energieInProzent = energieInProzent;
        this.schildInProzent = schildInProzent;
        this.lifeSupportInProzent = lifeSupportInProzent;
    }

    public double getEnergieInProzent() {
        return energieInProzent;
    }

    public void setEnergieInProzent(double energieInProzent) {
        this.energieInProzent = energieInProzent;
    }

    public double getSchildInProzent() {
        return schildInProzent;
    }

    public void setSchildInProzent(double schildInProzent) {
        this.schildInProzent = schildInProzent;
    }

    public double getLifeSupportInProzent() {
        return lifeSupportInProzent;
    }

    public void setLifeSupportInProzent(double lifeSupportInProzent) {
        this.lifeSupportInProzent = lifeSupportInProzent;
    }

    @Override
    public String toString() {
        return "StatusReport{" +
                "energieInProzent=" + energieInProzent +
                ", schildInProzent=" + schildInProzent +
                ", lifeSupportInProzent=" + lifeSupportInProzent +
                '}';
    }
}
