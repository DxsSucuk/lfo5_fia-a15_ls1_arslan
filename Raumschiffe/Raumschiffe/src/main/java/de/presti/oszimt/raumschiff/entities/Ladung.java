package de.presti.oszimt.raumschiff.entities;

public class Ladung {

    private String bezeichnung;
    private int anzahl;

    public Ladung(String bezeichnung, int anzahl) {
        this.bezeichnung = bezeichnung;
        this.anzahl = anzahl;
    }

    public String getBezeichnung() {
        return bezeichnung;
    }

    public void setBezeichnung(String bezeichnung) {
        this.bezeichnung = bezeichnung;
    }

    public int getAnzahl() {
        return anzahl;
    }

    public void setAnzahl(int anzahl) {
        this.anzahl = anzahl;
    }

    @Override
    public String toString() {
        return "Ladung{" +
                "bezeichnung='" + bezeichnung + '\'' +
                ", anzahl=" + anzahl +
                '}';
    }
}
