package de.presti.oszimt.raumschiff.entities;

public class LogbuchEintrag {

    private String title,
                    inhalt;

    public LogbuchEintrag(String title, String inhalt) {
        this.title = title;
        this.inhalt = inhalt;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getInhalt() {
        return inhalt;
    }

    public void setInhalt(String inhalt) {
        this.inhalt = inhalt;
    }

    @Override
    public String toString() {
        return "LogbuchEintrag{" +
                "title='" + title + '\'' +
                ", inhalt='" + inhalt + '\'' +
                '}';
    }
}
