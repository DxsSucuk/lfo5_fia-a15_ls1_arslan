package de.presti.oszimt.raumschiff.entities;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

public class Raumschiff {

    private String name;
    private Pilot pilot;

    private double energieVersorgungInProzent,
            schildInProzent,
            lifeSupportInProzent;

    private int repairBots,
            torpedos;

    private final ArrayList<Ladung> ladungen = new ArrayList<>();

    public Raumschiff(String name, Pilot pilot, double energieVersorgungInProzent, double schildInProzent,
                      double lifeSupportInProzent, int repairBots, int torpedos) {
        this.name = name;
        this.pilot = pilot;
        this.energieVersorgungInProzent = energieVersorgungInProzent;
        this.schildInProzent = schildInProzent;
        this.lifeSupportInProzent = lifeSupportInProzent;
        this.repairBots = repairBots;
        this.torpedos = torpedos;
    }

    public boolean schiessen(Raumschiff raumschiff) {
        raumschiff.damage(ThreadLocalRandom.current().nextDouble(raumschiff.getSchildInProzent() / 2));

        if (ThreadLocalRandom.current().nextBoolean()) {
            raumschiff.setLifeSupportInProzent(raumschiff.getLifeSupportInProzent() * 0.05);
            if (raumschiff.getLifeSupportInProzent() <= 15.0) {
                raumschiff.setLifeSupportInProzent(0);
            }
        }

        if (ThreadLocalRandom.current().nextBoolean()) {
            raumschiff.setEnergieVersorgungInProzent(raumschiff.getEnergieVersorgungInProzent() * 0.15);
            if (raumschiff.getEnergieVersorgungInProzent() <= 15.0) {
                raumschiff.setEnergieVersorgungInProzent(0);
            }
        }

        return true;
    }

    public boolean schiessenTorpedos(Raumschiff raumschiff, int anzahl) {
        if (getTorpedos() < anzahl) return false;

        for (int i = 0; i < anzahl; i ++) {
            if (ThreadLocalRandom.current().nextBoolean() && raumschiff.getSchildInProzent() > 1) {
                raumschiff.damage(ThreadLocalRandom.current().nextDouble(raumschiff.getSchildInProzent()));

                if (ThreadLocalRandom.current().nextBoolean()) {
                    raumschiff.setLifeSupportInProzent(raumschiff.getLifeSupportInProzent() * 0.15);
                    if (raumschiff.getLifeSupportInProzent() <= 15.0) {
                        raumschiff.setLifeSupportInProzent(0);
                    }
                }

                if (ThreadLocalRandom.current().nextBoolean()) {
                    raumschiff.setEnergieVersorgungInProzent(raumschiff.getEnergieVersorgungInProzent() * 0.25);
                    if (raumschiff.getEnergieVersorgungInProzent() <= 15.0) {
                        raumschiff.setEnergieVersorgungInProzent(0);
                    }
                }
            } else {
                // missed.
            }
        }

        setTorpedos(getTorpedos() - anzahl);

        return true;
    }

    public void damage(double damage) {
        setSchildInProzent(getSchildInProzent() - damage);
        if (getSchildInProzent() < 1.0) setSchildInProzent(0);
    }

    public void plundern(Raumschiff raumschiff) {
        if (raumschiff.kannGeplundertWerden()) {
            this.addLadung(raumschiff.getLadungen().toArray(Ladung[]::new));
            this.setTorpedos(this.getTorpedos() + ThreadLocalRandom.current().nextInt(raumschiff.getTorpedos()));
            this.setRepairBots(this.getRepairBots() + ThreadLocalRandom.current().nextInt(raumschiff.getRepairBots()));
        }
    }

    public boolean kannGeplundertWerden() {
        return (getSchildInProzent() < 1.0 && getLifeSupportInProzent() < 15.0 && getEnergieVersorgungInProzent() < 15.0) || getEnergieVersorgungInProzent() <= 0 || getLifeSupportInProzent() <= 0;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public StatusReport getStatus() {
        return new StatusReport(this);
    }

    public Pilot getPilot() {
        return pilot;
    }

    public void setPilot(Pilot pilot) {
        this.pilot = pilot;
    }

    public double getEnergieVersorgungInProzent() {
        return energieVersorgungInProzent;
    }

    public void setEnergieVersorgungInProzent(double energieVersorgungInProzent) {
        this.energieVersorgungInProzent = energieVersorgungInProzent;
        if (energieVersorgungInProzent <= 0) {
            setSchildInProzent(0);
            setLifeSupportInProzent(0);
        }
    }

    public double getSchildInProzent() {
        return schildInProzent;
    }

    public void setSchildInProzent(double schildInProzent) {
        this.schildInProzent = schildInProzent;
    }

    public double getLifeSupportInProzent() {
        return lifeSupportInProzent;
    }

    public void setLifeSupportInProzent(double lifeSupportInProzent) {
        this.lifeSupportInProzent = lifeSupportInProzent;
    }

    public int getRepairBots() {
        return repairBots;
    }

    public void setRepairBots(int repairBots) {
        this.repairBots = repairBots;
    }

    public int getTorpedos() {
        return torpedos;
    }

    public void setTorpedos(int torpedos) {
        this.torpedos = torpedos;
    }

    public ArrayList<Ladung> getLadungen() {
        return ladungen;
    }

    public boolean addLadung(Ladung ladung) {
        return !getLadungen().contains(ladung) && getLadungen().add(ladung);
    }

    public boolean addLadung(Ladung... ladung) {
        return getLadungen().addAll(List.of(ladung));
    }

    public boolean removeLadung(Ladung ladung) {
        return getLadungen().contains(ladung) && getLadungen().remove(ladung);
    }

    @Override
    public String toString() {
        return "Raumschiff{" +
                "name =" + name +
                ", pilot=" + pilot +
                ", energieVersorgungInProzent=" + energieVersorgungInProzent +
                ", schildInProzent=" + schildInProzent +
                ", lifeSupportInProzent=" + lifeSupportInProzent +
                ", repairBots=" + repairBots +
                ", torpedos=" + torpedos +
                ", ladungen=" + ladungen +
                '}';
    }
}
