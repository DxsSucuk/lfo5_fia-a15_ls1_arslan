import java.util.Scanner;

public class Rabatt {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Wie viel müssen Sie zahlen?");
        double betrag = scanner.nextDouble();



        if (betrag > 0) {
            if (betrag <= 100) {
                System.out.println("Sie erhalten ein Rabatt von 10%");
                betrag *= 0.90;
                System.out.println("Ihr Endbetrag beträgt: " + betrag);
            } else if (betrag <= 500) {
                System.out.println("Sie erhalten ein Rabatt von 15%");
                betrag *= 0.85;
                System.out.println("Ihr Endbetrag beträgt: " + betrag);
            } else {
                System.out.println("Sie erhalten ein Rabatt von 20%");
                betrag *= 0.80;
                System.out.println("Ihr Endbetrag beträgt: " + betrag);
            }
        } else {
            System.out.println("Betrag ist unter 0€");
        }

        scanner.close();
    }

}
