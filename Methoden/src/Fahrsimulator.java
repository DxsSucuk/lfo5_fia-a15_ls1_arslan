import java.util.Scanner;

public class Fahrsimulator {
	
	public static void main(String[] args) {
		
		Scanner scanner = new Scanner(System.in);
		
		double v = 0;
		
		while(scanner.hasNext()) {
			v = beschleunige(v, scanner.nextDouble());
			System.out.println("Die jetzige Geschwindigkeit betr�gt: " + v + ".");
		}
		
		scanner.close();
	}
	
	static double beschleunige(double v, double dv) {
		double speed = v + dv;
		
		if (dv > 0)
			System.out.println("Beschleunige");
		else
			System.out.println("Bremse");
		
		return (speed > 130 ? 130 : speed < 0 ? 0 : speed);
	}

}
