import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		Scanner myScanner = new Scanner(System.in);
		
		// Benutzereingaben lesen
		System.out.println("was m�chten Sie bestellen?");
		String artikel = liesString(myScanner.next());
		
		System.out.println("Geben Sie die Anzahl ein:");
		int anzahl = liesInt(myScanner.next());

		System.out.println("Geben Sie den Nettopreis ein:");
		double preis = liesDouble(myScanner.next());

		System.out.println("Geben Sie den Mehrwertsteuersatz in Prozent ein:");
		double mwst = liesDouble(myScanner.next());

		// Verarbeiten
		double nettogesamtpreis = berechneGesamtNettopreis(anzahl, preis);
		double bruttogesamtpreis = berechneGesamtBruttopreis(nettogesamtpreis, mwst);
		
		rechnungAusgeben(artikel, anzahl, nettogesamtpreis, bruttogesamtpreis, mwst);
		
		myScanner.close();
	}
	
	public static String liesString(String text) {
		return text;
	}
	
	public static int liesInt(String text) {
		return Integer.parseInt(text);
	}
	
	public static double liesDouble(String text) {
		return Double.parseDouble(text);
	}
	
	public static double berechneGesamtNettopreis(int anzahl, double nettoPreis) {
		return (double)anzahl * nettoPreis;
	}
	
	public static double berechneGesamtBruttopreis(double nettoPreis, double mwst) {
		return nettoPreis * (1 + mwst / 100);
	}
	
	public static void rechnungAusgeben(String artikel, int anzahl, double nettogesamtpreis, double bruttogesamtpreis, double mwst) {
		System.out.println("\tRechnung");
		System.out.printf("\t\t Netto:  %-20s %6d %10.2f %n", artikel, anzahl, nettogesamtpreis);
		System.out.printf("\t\t Brutto: %-20s %6d %10.2f (%.1f%s)%n", artikel, anzahl, bruttogesamtpreis, mwst, "%");
	}
}
