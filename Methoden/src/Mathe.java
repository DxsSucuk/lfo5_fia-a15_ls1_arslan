
public class Mathe {

	// Aufgabe 5
	public static double quadrat(double x) {
		return (x * x);
	}
	
	// Aufgabe 6
	public double hypotenuse(double kathete1,
			double kathete2) {
		return Math.sqrt((quadrat(kathete1) + quadrat(kathete2)));
	}
	
	// Aufgabe 3
	static double reihenschaltung(double r, double r2) {
		return r + r2;
	}
	
	// Aufgabe 4
	static double parallelschaltung(double r, double r2) {
		return (r * r2) / (r + r2);
	}
}
