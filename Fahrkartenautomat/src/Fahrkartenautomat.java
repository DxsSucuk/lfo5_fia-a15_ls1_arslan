import java.util.Scanner;

class Fahrkartenautomat {
	static Scanner tastatur = new Scanner(System.in);
	
    public static void main(String[] args) {

        while (tastatur != null) {
            double zuZahlenderBetrag = fahrkartenbestellungErfassen(), ruckgabebetrag = fahrkartenBezahlen(zuZahlenderBetrag);

            fahrkartenAusgeben();

            rueckgeldAusgeben(ruckgabebetrag);

            try {
                Thread.sleep(1000);
            } catch (Exception ignore) {}

            System.out.println("\nVergessen Sie nicht, den Fahrschein\n" +
                    "vor Fahrtantritt entwerten zu lassen!\n" +
                    "Wir wünschen Ihnen eine gute Fahrt.\n\n\n");
        }

        tastatur.close();
    }
    
    public static double fahrkartenbestellungErfassen() {
        double zuZahlenderBetrag;
        int anzahlTickets = -1;

        String[] ticketNames = new String[] { "Einzelfahrschein Berlin AB", "Einzelfahrschein Berlin BC", "Einzelfahrschein Berlin ABC", "Kurzstrecke", "Tageskarte Berlin AB",
                "Tageskarte Berlin BC", "Tageskarte Berlin ABC", "Kleingruppen-Tageskarte Berlin AB", "Kleingruppen-Tageskarte Berlin BC", "Kleingruppen-Tageskarte Berlin ABC" };

        double[] ticketKosten = new double[] { 2.90, 3.30, 3.60, 1.90, 8.60, 9.0, 9.6, 23.5, 24.30, 23.90 };

        for (int i = 0; i < ticketNames.length; i++) {
            System.out.printf("%s [%.2f EUR] (%d)%n", ticketNames[i], ticketKosten[i], i + 1);
        }

        System.out.print("Ticket Auswahl: ");
        int selectedIndex = tastatur.nextInt();

        if (selectedIndex <= 0 || ticketNames.length < (selectedIndex - 1)) {
            System.out.println("Fehlerhafter Eintrag!.");
            return fahrkartenbestellungErfassen();
        }

        zuZahlenderBetrag = ticketKosten[selectedIndex - 1];

        while (anzahlTickets == -1) {
            System.out.print("Ticket Anzahl: ");
            // Wert zuweisung des Integers.
            anzahlTickets = tastatur.nextInt();
            if (anzahlTickets <= 0 || anzahlTickets > 10) {
                System.out.println(">> Wählen Sie bitte eine Anzahl von 1 bis 10 Tickets aus.");
                System.out.println();
                anzahlTickets = -1;
            }
        }
        
        // Das Bearbeiten des end Betrags.
        return zuZahlenderBetrag * anzahlTickets;
    }
    
    public static double fahrkartenBezahlen(double zuZahlenderBetrag) {
        // Geldeinwurf
        // -----------
        double eingezahlterGesamtbetrag = 0.0;
        while (eingezahlterGesamtbetrag < zuZahlenderBetrag) {
            System.out.println("Noch zu zahlen: " + String.format("%.2f Euro", (zuZahlenderBetrag - eingezahlterGesamtbetrag)));
            System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
            eingezahlterGesamtbetrag += tastatur.nextDouble();
        }
        
        return eingezahlterGesamtbetrag - zuZahlenderBetrag;
    }
    
    public static void fahrkartenAusgeben() {
        // Fahrscheinausgabe
        // -----------------
        System.out.println("\nFahrschein wird ausgegeben");
        for (int i = 0; i < 8; i++) {
            System.out.print("=");
            try {
                Thread.sleep(250);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        System.out.println("\n\n");
    }
    
    public static void rueckgeldAusgeben(double ruckgabebetrag) {
        // R�ckgeldberechnung und -Ausgabe
        // -------------------------------
        
        if (ruckgabebetrag > 0.0) {
            System.out.println("Der Rückgabebetrag in Höhe von " + String.format("%.2f Euro", ruckgabebetrag));
            System.out.println("wird in folgenden Münzen ausgezahlt:");

            int zweiEuro = 0, einEuro = 0, fuenfzigCent = 0, zwanzigCent = 0, zehnCent = 0, fuenfCent = 0;

            while (ruckgabebetrag >= 2.0) // 2 EURO-M�nzen
            {
                zweiEuro++;
                ruckgabebetrag -= 2.0;
            }
            while (ruckgabebetrag >= 1.0) // 1 EURO-M�nzen
            {
                einEuro++;
                ruckgabebetrag -= 1.0;
            }
            while (ruckgabebetrag >= 0.5) // 50 CENT-M�nzen
            {
                fuenfzigCent++;
                ruckgabebetrag -= 0.5;
            }
            while (ruckgabebetrag >= 0.2) // 20 CENT-M�nzen
            {
                zwanzigCent++;
                ruckgabebetrag -= 0.2;
            }
            while (ruckgabebetrag >= 0.1) // 10 CENT-M�nzen
            {
                zehnCent++;
                ruckgabebetrag -= 0.1;
            }
            while (ruckgabebetrag >= 0.05)// 5 CENT-M�nzen
            {
                fuenfCent++;
                ruckgabebetrag -= 0.05;
            }

            if (zweiEuro > 0) System.out.println(zweiEuro + " mal 2 Euro Stück" + (zweiEuro > 1 ? "e" : "") + ".");
            if (einEuro > 0) System.out.println(einEuro + " mal 1 Euro Stück" + (einEuro > 1 ? "e" : "") + ".");
            if (fuenfzigCent > 0) System.out.println(fuenfzigCent + " mal 50 Cent Stück" + (fuenfzigCent > 1 ? "e" : "") + ".");
            if (zwanzigCent > 0) System.out.println(zwanzigCent + " mal 20 Cent Stück" + (zwanzigCent > 1 ? "e" : "") + ".");
            if (zehnCent > 0) System.out.println(zehnCent + " mal 10 Cent Stück" + (zehnCent > 1 ? "e" : "") + ".");
            if (fuenfCent > 0) System.out.println(fuenfCent + " mal 5 Cent Stück" + (fuenfCent > 1 ? "e" : "") + ".");

        }
    }
}