import java.time.Month;
import java.time.format.TextStyle;
import java.util.Locale;
import java.util.Scanner;

public class MonatGetter {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Monat bitte angeben:");
        int i = scanner.nextInt();

        Month month = Month.of(i);

        if (month == null) {
            System.out.println("Fehlerhaft.");
            return;
        }

        System.out.println(month.getDisplayName(TextStyle.FULL, Locale.GERMANY));

        scanner.close();
    }
}
