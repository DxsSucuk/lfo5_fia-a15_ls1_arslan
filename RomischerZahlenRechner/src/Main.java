import java.util.Scanner;

public class Main {

    public static void main(String args[]) {

        Scanner scanner = new Scanner(System.in);

        System.out.println("Bitte geben Sie den Römischen String an:");

        String rmString = scanner.next();

        addieren(rmString);

    }

    private static void addieren(String rmString) {

        int endZahl = 0;

        for (int i = 0; i < rmString.length(); i++) {

            if (rmString.charAt(i) == 'I') {
                if (rmString.length() >= i + 2 && rmString.charAt(i + 1) != 'I') {
                    if (rmString.length() >= i + 3 && rmString.charAt(i + 2) != rmString.charAt(i + 1)) {
                        endZahl += 1;
                    } else {
                        endZahl -= 1;
                    }
                } else {
                    endZahl += 1;
                }
            } else if (rmString.charAt(i) == 'V') {
                if (rmString.length() >= i + 2 && rmString.charAt(i + 1) != 'I' && rmString.charAt(i + 1) != 'V') {
                    if (rmString.length() >= i + 3 && rmString.charAt(i + 2) != rmString.charAt(i + 1) && rmString.charAt(i + 2) != 'I') {
                        endZahl += 5;
                    } else {
                        endZahl -= 5;
                    }
                } else {
                    endZahl += 5;
                }
            } else if (rmString.charAt(i) == 'X') {
                if (rmString.length() >= i + 2 && rmString.charAt(i + 1) != 'I' && rmString.charAt(i + 1) != 'V' && rmString.charAt(i + 1) != 'X') {
                    if (rmString.length() >= i + 3 && rmString.charAt(i + 2) != rmString.charAt(i + 1) && rmString.charAt(i + 2) != 'I' && rmString.charAt(i + 2) != 'V') {
                        endZahl += 10;
                    } else {
                        endZahl -= 10;
                    }
                } else {
                    endZahl += 10;
                }
            } else if (rmString.charAt(i) == 'L') {
                if (rmString.length() >= i + 2 && rmString.charAt(i + 1) != 'I' && rmString.charAt(i + 1) != 'V' && rmString.charAt(i + 1) != 'X' &&
                        rmString.charAt(i + 1) != 'L') {
                    if (rmString.length() >= i + 3 && rmString.charAt(i + 2) != rmString.charAt(i + 1) && rmString.charAt(i + 2) != 'I' && rmString.charAt(i + 2) != 'V' && rmString.charAt(i + 2) != 'X') {
                        endZahl += 50;
                    } else {
                        endZahl -= 50;
                    }

                } else {
                    endZahl += 50;
                }
            } else if (rmString.charAt(i) == 'C') {
                if (rmString.length() >= i + 2 && rmString.charAt(i + 1) != 'I' && rmString.charAt(i + 1) != 'V' && rmString.charAt(i + 1) != 'X' &&
                        rmString.charAt(i + 1) != 'L' && rmString.charAt(i + 1) != 'C') {
                    if (rmString.length() >= i + 3 && rmString.charAt(i + 2) != rmString.charAt(i + 1) && rmString.charAt(i + 2) != 'I' && rmString.charAt(i + 2) != 'V' && rmString.charAt(i + 2) != 'X' && rmString.charAt(i + 2) == 'C') {
                        endZahl += 100;
                    } else {
                        endZahl -= 100;
                    }
                } else {
                    endZahl += 100;
                }
            } else if (rmString.charAt(i) == 'D') {
                if (rmString.length() >= i + 2 && rmString.charAt(i + 1) != 'I' && rmString.charAt(i + 1) != 'V' && rmString.charAt(i + 1) != 'X' &&
                        rmString.charAt(i + 1) != 'L' && rmString.charAt(i + 1) != 'C' && rmString.charAt(i + 1) != 'D') {
                    if (rmString.length() >= i + 3 && rmString.charAt(i + 2) != rmString.charAt(i + 1) && rmString.charAt(i + 2) != 'I' && rmString.charAt(i + 2) != 'V' && rmString.charAt(i + 2) != 'X' && rmString.charAt(i + 2) == 'C' && rmString.charAt(i + 2) != 'D') {
                        endZahl += 500;
                    } else {
                        endZahl -= 500;
                    }

                } else {
                    endZahl += 500;
                }
            } else if (rmString.charAt(i) == 'M') {
                if (rmString.length() >= i + 2 && rmString.charAt(i + 1) != 'I' && rmString.charAt(i + 1) != 'V' && rmString.charAt(i + 1) != 'X' &&
                        rmString.charAt(i + 1) != 'L' && rmString.charAt(i + 1) != 'C' && rmString.charAt(i + 1) != 'D' && rmString.charAt(i + 1) != 'M') {
                    if (rmString.length() >= i + 3 && rmString.charAt(i + 2) != rmString.charAt(i + 1) && rmString.charAt(i + 2) != 'I' && rmString.charAt(i + 2) != 'V' && rmString.charAt(i + 2) != 'X' && rmString.charAt(i + 2) == 'C' && rmString.charAt(i + 2) != 'D' && rmString.charAt(i + 2) != 'M') {
                        endZahl += 1000;
                    } else {
                        endZahl -= 1000;
                    }
                } else {
                    endZahl += 1000;
                }
            } else {
                System.out.println("Unknown Code");
            }
        }
        System.out.println(endZahl);
    }

}
