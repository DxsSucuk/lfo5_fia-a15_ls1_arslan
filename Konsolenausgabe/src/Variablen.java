import java.util.Scanner;

/** Variablen.java
    Ergaenzen Sie nach jedem Kommentar jeweils den Quellcode.
    @author
    @version
*/
public class Variablen {

  static int durchlauf;

  public static void main(String [] args){
    durchlauf = 25;

    System.out.println(durchlauf);

    Scanner scanner = new Scanner(System.in);

    char inputCharacter = scanner.nextLine().charAt(0);

    // TODO Irgendwas mit einem Men�punkt? (3.)

    inputCharacter = 'C';

    System.out.println(inputCharacter);

    long grosseZahl;

    grosseZahl = 299792458L;

    System.out.println(grosseZahl);

    int vereinsGruender = 7;

    System.out.println(vereinsGruender);

    double elementareladung = 1.602 * (10^-19);

    System.out.println(elementareladung);

    boolean paymentSuccess;

    paymentSuccess = true;

    System.out.println(paymentSuccess);

    scanner.close();
  }
}