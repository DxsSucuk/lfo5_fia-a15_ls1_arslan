
public class Main {
	
	public static void main(String[] args) {
		Aufgabe3AB6();
	}
	
	
	//AB1.5
	public static void Aufgabe1() {
		System.out.print("Das ist ein \"Beispielsatz\". ");
		System.out.println("Ein Beispielsatz ist das.");
		System.out.println("Das ist ein \"Beispielsatz\"\nEin Beispielsatz ist das.");
	}
	
	public static void Aufgabe2() {
		System.out.println("    *    \n" +
	                       "   ***   \n" +
				           "  *****  \n" +
	                       " ******* \n" +
				           "*********\n" +
	                       "   ***   \n" +
                           "   ***   \n");
	}
	
	public static void Aufgabe3() {
		System.out.printf("%.2f", 22.4234234);
		System.out.println();
		System.out.printf("%.2f", 111.2222);
		System.out.println();
		System.out.printf("%.2f", 4.0);
		System.out.println();
		System.out.printf("%.2f", 1000000.551);
		System.out.println();
		System.out.printf("%.2f", 97.34);
	}
	
	//AB1.6
	public static void Aufgabe1AB6() {
		
		//Top
		System.out.printf("%2.1s", "*");
		System.out.printf("%3.1s%n", "*");
		
		//Middle Upper
		System.out.printf("%1.1s", "*");
		System.out.printf("%5.1s%n", "*");
		
		//Middle Lower
		System.out.printf("%1.1s", "*");
		System.out.printf("%5.1s%n", "*");
		
		//Bottom
		System.out.printf("%2.1s", "*");
		System.out.printf("%3.1s%n", "*");
	}
	
	public static void Aufgabe2AB6() {
		for (int i = 0; i < 10; i++) {
			int x = i;

			String numbers = "";

			for (int j = 1; j < i; j++) {
				x = x * j;
				if (j != 1) {
					numbers += " * " + j;
				} else {
					numbers += j + " ";
				}
			}

			if (numbers.isEmpty()) {
				numbers += "" + i;
			} else {
				numbers += " * " +i;
			}

			System.out.printf("%-5s = %-19s = %4s%n", i + "!", numbers, "" + x);
		}
	}
	
	public static void Aufgabe3AB6() {
		System.out.printf("%-12s | %10s%n", "Fahrenheit", "Celsius");
		for (int i = 0; i < 25; i++) {
			System.out.print("-");
		}
		System.out.print("\n");
		System.out.printf("%-12s | %10.2f%n", "-20", -28.8889);
		System.out.printf("%-12s | %10.2f%n", "-10", -23.3333);
		System.out.printf("%-12s | %10.2f%n", "+0", -17.7778);
		System.out.printf("%-12s | %10.2f%n", "+20", -6.6667);
		System.out.printf("%-12s | %10.2f%n", "+30", -1.1111);
	}

	public static void Aufgabe2AB1() {

	}
}