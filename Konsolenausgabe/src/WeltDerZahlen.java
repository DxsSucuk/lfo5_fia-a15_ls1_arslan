public class WeltDerZahlen {

    public static void main(String[] args) {
    /*  *********************************************************
    Zuerst werden die Variablen mit den Werten festgelegt!
    *********************************************************** */
        // Im Internet gefunden ?
        // Die Anzahl der Planeten in unserem Sonnesystem
        int anzahlPlaneten =  8;

        // Anzahl der Sterne in unserer Milchstraße
        long anzahlSterne = 400000000000L;

        // Wie viele Einwohner hat Berlin?
        int bewohnerBerlin = 3645000;

        // Wie alt bist du? Wie viele Tage sind das?
        short alterTage = 5840;

        // Wie viel wiegt das schwerste Tier der Welt?
        // Schreiben Sie das Gewicht in Kilogramm auf!
        int gewichtKilogramm = 150000;

        // Schreiben Sie auf, wie viele km² das größte Land er Erde hat?
        int flaecheGroessteLand = 17100000;

        // Wie groß ist das kleinste Land der Erde?
        float flaecheKleinsteLand = 0.44F;

    /*  *********************************************************
    Programmieren Sie jetzt die Ausgaben der Werte in den Variablen
    *********************************************************** */

        System.out.println("Anzhahl der Planeten: " + anzahlPlaneten);

        System.out.println("Anzahl der Sterne: " + anzahlSterne);

        System.out.println("Bewohner in Berlin: " + bewohnerBerlin);

        System.out.println("Mein Alter in Tagen: " + alterTage);

        System.out.println("Das Gewicht des Schwersten Tieres: " + gewichtKilogramm);

        System.out.println("Die größe des größten Landes in Quadratkilometer: " + flaecheGroessteLand);

        System.out.println("Die größe des kleinsten Landes in Quadratkilometer: " + flaecheKleinsteLand);
    }

}
